//
//  Config.swift
//  D2p
//
//  Created by Mohanned on 1/29/18.
//  Copyright © 2018 Mohanned. All rights reserved.
//


import Foundation
import SwiftyJSON

/*
 {
     "id": 12,
     "user_id": "1",
     "photo": "uploads\/photos\/148652543229083.jpg",

 }
 */
class Photo: NSObject {
    var id: Int
    var photo: String
    var name: String
    
    init?(dict: [String: JSON]) {
        guard let id = dict["id"]?.toInt, let name = dict["name"]?.string , let photo = dict["photo"]?.toImagePath, !photo.isEmpty else { return nil }
        
        self.id = id
        self.url = photo
    }
}

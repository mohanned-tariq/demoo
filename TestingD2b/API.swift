//
//  Config.swift
//  D2p
//
//  Created by Mohanned on 1/28/18.
//  Copyright © 2018 Mohanned. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


class API {

    class func login(username: String, password: String, completion: @escaping (_ error: Error?, _ success: Bool)->Void) {
        let url = URLs.login
        
        let parameters = [
            "username": username,
            "password": password
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                switch response.result
                {
                case .failure(let error):
                    completion(error, false)
                    print(error)
                    
                case .success(let value):
                    let json = JSON(value)
                   
                     print("Json", json)
                   
                    if let api_token = json["api_token"].string {
                      
                        print("api_token: \(api_token)")
                        
                       // save api_token to UserDefaults
         
                        helper.saveApiToken (token: api_token)
                        
                        completion(nil, true)
                        
                        
//                        let default = UserDefaults.standard
//                        default.set(accessToken, forKey: "accessToken")
//                        default.synchronized()
//                        //Now get like this and use guard so that it will prevent your crash if value is nil.
//                        guard let accessTokenValue = default.string(forKey: "accessToken")
//                        else {return}
//                        print(accessTokenValue)
                        
            
                        
                        
                    }
                }
                
        }
    }
    
    class func register(name: String, email: String, password: String, completion: @escaping (_ error: Error?, _ success: Bool)->Void) {
        let url = URLs.register
        
        let parameters = [
            "name": name,
            "email": email,
            "password": password,
            "password_confirmation": password
        ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: URLEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                switch response.result
                {
                case .failure(let error):
                    completion(error, false)
                    print(error)
                    
                case .success(let value):
                    let json = JSON(value)
                    print("Register Json" , json)
                    if let api_token = json["result"]["api_token"].string {
                        
                        print("api_token: \(api_token)")
                        
                        // save api token to UserDefaults
                        helper.saveApiToken(token: api_token)
                        
                        completion(nil, true)
                    }
                }
                
        }
    }
    
    
}

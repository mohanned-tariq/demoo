//
//  Config.swift
//  D2p
//
//  Created by Mohanned on 1/28/18.
//  Copyright © 2018 Mohanned. All rights reserved.
//

import UIKit


// [{"key":"username","value":"memo@gmail.com","description":""}]

class LoginVC: UIViewController {
    
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    @IBAction func loginPressed(_ sender: UIButton) {
        guard let username = emailTF.text, !username.isEmpty else { return }
        guard let password = passwordTF.text, !password.isEmpty else { return }
       
        API.login(username: username, password: password) { (error: Error?, success: Bool) in
            if success == true {
                print("Sucess User ")
            } else {
                // say sorry to user and try again
            }
        }
    }
}













